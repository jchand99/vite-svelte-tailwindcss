/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}", "./src/**/*.svelte", "index.html"],
  theme: {
    extend: {},
  },
  plugins: [],
}
